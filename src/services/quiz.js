import axios from 'axios';
import config from './config';

const client = axios.create({
  baseURL: config.baseURL,
});

export function getQuestions(amount = 10, difficulty = 'hard', type = 'boolean') {
  return client.get(`?amount=${amount}&difficulty=${difficulty}&type=${type}`);
}
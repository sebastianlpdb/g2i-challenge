import React from 'react';
import PropTypes from 'prop-types';

export default function Question(props) {
  return (
    <div className="card question">
      {props.text}
    </div>
  );
}

Question.propTypes = {
  text: PropTypes.string.isRequired
};
import React from 'react';
import PropTypes from 'prop-types';

export default function SectionTitle(props) {
  return (
    <h1>{props.title}</h1>
  );
}

SectionTitle.propTypes = {
  title: PropTypes.string.isRequired
};
import React from 'react';
import PropTypes from 'prop-types';

export default function QuestionAnswers(props) {
  return (
    <div className="question-answers">
      {
        props.answers.map(answer => {
          return <button value={answer} key={answer} onClick={props.onClick}>{answer}</button>;
        })
      }
    </div>
  );
}

QuestionAnswers.propTypes = {
  answers: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  onClick: PropTypes.func.isRequired
};
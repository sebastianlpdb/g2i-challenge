import React from 'react';
import PropTypes from 'prop-types';

export default function GameExplanation(props) {
  return (
    <div className="game-explanation">
      <div className="main-text">
        <h3>{props.main}</h3>
      </div>
      <div className="secondary-text">
        <h4>{props.secondary}</h4>
      </div>
    </div>
  );
}

GameExplanation.propTypes = {
  main: PropTypes.string.isRequired,
  secondary: PropTypes.string
};

GameExplanation.defaultProps = {
  secondary: ''
}
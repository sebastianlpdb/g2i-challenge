import { Component } from 'react';
import GetProTotypeChain from 'get-prototype-chain';

export default class BaseComponent extends Component {
  constructor(props) {
    super(props);

    const chain = GetProTotypeChain(this);
    const methods = chain.reduce((previous, current) => previous.concat(Object.getOwnPropertyNames(current)), []);

    const excludes = ['constructor', 'isMounted', 'replaceState'] // remove deprecated methods

    methods.forEach(name => {
      if (excludes.indexOf(name) === -1) {
        const method = this[name];

        if (method instanceof Function) {
          this[name] = this[name].bind(this);
        }
      }
    })
  }
}
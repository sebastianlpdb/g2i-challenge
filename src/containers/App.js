import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import BaseComponent from './BaseComponent';
import Home from './Home';
import Wrapper from './Wrapper';

class App extends BaseComponent {  
  render() {
    return (
      <Router>
        <div className="container">
          <Route exact path="/" component={Home} />
          <Route path="/quiz" component={Wrapper} />
        </div>
      </Router>
    );
  }
}

export default App;

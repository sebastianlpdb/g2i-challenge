import React from 'react';
import BaseComponent from './BaseComponent';
import SectionTitle from '../components/SectionTitle';
import GameExplanation from '../components/GameExplanation';
import { Link } from 'react-router-dom';

class Home extends BaseComponent {  
  render() {
    return (
      <div className="home">
        <SectionTitle title="Welcome to the Trivia Challenge"/>
        <GameExplanation main="You will be presented with 10 True or False questions." secondary="Can you score 100%?" />
        <Link to="/quiz">BEGIN</Link>
      </div>
    );
  }
}

export default Home;

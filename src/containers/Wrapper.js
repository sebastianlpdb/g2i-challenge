import React from 'react';
import BaseComponent from './BaseComponent';
import Results from './Results';
import Quiz from './Quiz';
import { Route, Redirect } from 'react-router-dom';

class Wrapper extends BaseComponent {
  constructor(props) {
    super(props);

    this.total = 10;

    this.state = {
      isFinished: false,
      correct: 0,
    };
  }

  handleLastQuestion() {
    this.setState({
      isFinished: true
    });
  }

  handleQuestionAnswer(answer) {
    if (answer) {
      this.setState(prevState => ({
        correct: prevState.correct + 1
      }));
    }
  }

  render() {
    return (
      <div className="quiz">
        {
          this.state.isFinished ? 
            <Results total={this.total} correct={this.state.correct} /> :
            <Quiz onLastQuestion={this.handleLastQuestion} onQuestionAnswer={this.handleQuestionAnswer} />
        }
      </div>
    );
  }
}

export default Wrapper;

import React from 'react';
import PropTypes from 'prop-types';
import Entities from 'entities';
import { getQuestions } from '../services/quiz';
import BaseComponent from './BaseComponent';
import QuestionTitle from '../components/SectionTitle';
import Question from '../components/Question';
import QuestionAnswers from '../components/QuestionAnswers';

class Quiz extends BaseComponent {
  constructor(props) {
    super(props);

    this.state = {
      questions: [],
      index: 0
    };
  }

  componentDidMount() {
    getQuestions().then(response => {
      console.log(response.data);
      this.setState({
        questions: response.data.results.map(question => {
          return {
            category: Entities.decodeHTML(question.category),
            question: Entities.decodeHTML(question.question),
            correctAnswer: question.correct_answer
          }
        })
      });
    });
  }

  handleAnswerButtonClick(event) {
    if (this.state.index === 9) {
      this.props.onLastQuestion()
    }

    this.props.onQuestionAnswer(event.target.value === this.state.questions[this.state.index].correctAnswer);

    this.setState(prevState => ({
      index: prevState.index + 1
    }));
  }

  render() {
    const { index, questions } = this.state;
    const length = this.state.questions.length;

    return (
      <div className="quiz">
        {
           (length > 0 && index < 10) &&
            <div className="quiz--container">
              <QuestionTitle title={questions[index].category} />
              <Question text={questions[index].question} />
              <QuestionAnswers answers={['True', 'False']} onClick={this.handleAnswerButtonClick} />
            </div>
        }
      </div>
    );
  }
}

Quiz.propTypes = {
  onLastQuestion: PropTypes.func.isRequired,
  onQuestionAnswer: PropTypes.func.isRequired
};

export default Quiz;

import React from 'react';
import BaseComponent from './BaseComponent';

class Results extends BaseComponent {  
  render() {
    return (
      <div className="results">
        <h3>{`${this.props.correct}/${this.props.total}`}</h3>
      </div>
    );
  }
}

export default Results;
